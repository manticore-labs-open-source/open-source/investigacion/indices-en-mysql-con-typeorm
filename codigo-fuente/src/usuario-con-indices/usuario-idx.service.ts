import {HttpStatus, Injectable} from '@nestjs/common';
import {InjectRepository} from '@nestjs/typeorm';
import {Like, Repository} from 'typeorm';
import {UsuarioConIndicesEntity} from "./usuario-con-indices.entity";

@Injectable()
export class UsuarioIdxService {
    constructor(
        @InjectRepository(UsuarioConIndicesEntity)
        private usuarioConIndicesRepository: Repository<UsuarioConIndicesEntity>
    ) {
    }

    async crear(usuario: UsuarioConIndicesEntity):
        Promise<UsuarioConIndicesEntity> {
        try {
            const usuarioCreado =
                await this.usuarioConIndicesRepository.save(usuario);
            return usuarioCreado;
        } catch (error) {
            throw {
                mensaje: 'No se pudo crear el usuario',
                codigo: HttpStatus.INTERNAL_SERVER_ERROR,
                error
            }
        }
    }

    async listarUsuariosActivosConIndices():
        Promise<UsuarioConIndicesEntity[]> {
        try {
            const usuariosActivos =
                await this.usuarioConIndicesRepository.find({
                    where: {
                        estaActivo: true
                    }
                });
            return usuariosActivos;
        } catch (error) {
            throw {
                mensaje: 'No se pudo listar los usuarios activos',
                codigo: HttpStatus.INTERNAL_SERVER_ERROR,
                error
            }
        }
    }

    async listarUsuariosConIndices():
        Promise<UsuarioConIndicesEntity[]> {
        try {
            const usuariosActivos =
                await this.usuarioConIndicesRepository.find();
            return usuariosActivos;
        } catch (error) {
            throw {
                mensaje: 'No se pudo listar los usuarios activos',
                codigo: HttpStatus.INTERNAL_SERVER_ERROR,
                error
            }
        }
    }

    async listarUsuariosConNombreIgualAConIndices(nombreABuscar: string):
        Promise<UsuarioConIndicesEntity[]> {
        try {
            const usuariosConNombreIgualA =
                await this.usuarioConIndicesRepository.find({
                    where: {
                        nombre: Like(nombreABuscar),
                    }
                })
            return usuariosConNombreIgualA;
        } catch (error) {
            throw {
                mensaje:
                    'No se pudo listar los usuarios con nombre igual a' + nombreABuscar,
                codigo: HttpStatus.INTERNAL_SERVER_ERROR,
                error
            }
        }
    }
}
