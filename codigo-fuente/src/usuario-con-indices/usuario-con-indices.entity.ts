import {Entity, Column, PrimaryGeneratedColumn, Index} from 'typeorm';

@Entity('usuario_con_indices')
@Index([
    'nombre',
    'apellido'
])
export class UsuarioConIndicesEntity {
    @PrimaryGeneratedColumn()
    id: number;

    @Index('inx_cedula',
        {
            unique: true
        })
    @Column()
    cedula: string;

    @Column()
    nombre: string;

    @Column()
    apellido: string;

    @Column()
    edad: number;

    @Index()
    @Column(
        {
            default: true
        }
    )
    estaActivo: boolean;
}
