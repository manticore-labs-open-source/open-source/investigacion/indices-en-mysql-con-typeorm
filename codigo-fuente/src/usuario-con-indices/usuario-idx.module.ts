import {Module} from '@nestjs/common';
import {TypeOrmModule} from '@nestjs/typeorm';
import {UsuarioConIndicesEntity} from "./usuario-con-indices.entity";
import {UsuarioIdxService} from "./usuario-idx.service";
import {UsuarioIdxController} from "./usuario-idx.controller";

@Module({
    imports: [
        TypeOrmModule.forFeature(
            [
                UsuarioConIndicesEntity,
            ],
            'default')
    ],
    providers: [UsuarioIdxService],
    controllers: [UsuarioIdxController],
    exports: [UsuarioIdxService]
})
export class UsuarioIdxModule {
}
