import {Module} from '@nestjs/common';
import {AppController} from './app.controller';
import {AppService} from './app.service';
import {UsuarioEntity} from "./usuario/usuario.entity";
import {TypeOrmModule} from "@nestjs/typeorm";
import {UsuarioModule} from "./usuario/usuario.module";
import {UsuarioIdxModule} from "./usuario-con-indices/usuario-idx.module";
import {UsuarioConIndicesEntity} from "./usuario-con-indices/usuario-con-indices.entity";
import {UsuarioService} from "./usuario/usuario.service";
import {UsuarioIdxService} from "./usuario-con-indices/usuario-idx.service";
import {CONFIGURACIONES} from "./constantes/configuraciones";
import {ARREGLO_ENTITIES} from "./constantes/arreglo.entities";
import {ARREGLO_MODULOS} from "./constantes/datos/arreglo.modulos";
import {FUNCIONES_GENERALES} from "./constantes/funciones.generales";

@Module({
    imports: [
        TypeOrmModule.forRoot({
            type: 'mysql',
            host: CONFIGURACIONES.bdd.host,
            port: CONFIGURACIONES.bdd.port,
            username: CONFIGURACIONES.bdd.username,
            password: CONFIGURACIONES.bdd.password,
            database: CONFIGURACIONES.bdd.database,
            entities: [...ARREGLO_ENTITIES],
            synchronize: CONFIGURACIONES.bdd.synchronize,
            dropSchema: CONFIGURACIONES.bdd.dropSchema,
        }),
        ...ARREGLO_MODULOS,
    ],
    controllers: [AppController],
    providers: [AppService],
})
export class AppModule {
    constructor(
        private readonly _usuarioService: UsuarioService,
        private readonly _usuarioIdxService: UsuarioIdxService
    ) {
        if (CONFIGURACIONES.crearDatosTest) {
            this.datos();
        } else {
            console.info('Datos no creados');
        }

    }

    async datos() {
        try {
            await FUNCIONES_GENERALES.crearDatos(
                'datos-usuario.json',
                this._usuarioService);
            await FUNCIONES_GENERALES.crearDatos(
                'datos-usuario.json',
                this._usuarioIdxService);
            console.info('Datos cargados correctamente');
        } catch (e) {
            console.error('Error al crear datos', e);
        }
    }

}
