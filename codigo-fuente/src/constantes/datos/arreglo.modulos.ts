import {UsuarioModule} from "../../usuario/usuario.module";
import {UsuarioIdxModule} from "../../usuario-con-indices/usuario-idx.module";

export const ARREGLO_MODULOS = [
    UsuarioModule,
    UsuarioIdxModule
]
