import {UsuarioConIndicesEntity} from "../usuario-con-indices/usuario-con-indices.entity";
import {UsuarioEntity} from "../usuario/usuario.entity";

export const ARREGLO_ENTITIES = [
    UsuarioEntity,
    UsuarioConIndicesEntity
]
