import {Entity, Column, PrimaryGeneratedColumn} from 'typeorm';

@Entity('usuario')
export class UsuarioEntity {
    @PrimaryGeneratedColumn()
    id: number;

    @Column()
    cedula: string;

    @Column()
    nombre: string;

    @Column()
    apellido: string;

    @Column()
    edad: number;

    @Column(
        {
            default: true
        }
    )
    estaActivo: boolean;
}
