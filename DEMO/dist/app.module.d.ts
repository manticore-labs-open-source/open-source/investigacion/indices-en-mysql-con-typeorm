import { UsuarioService } from "./usuario/usuario.service";
import { UsuarioIdxService } from "./usuario-con-indices/usuario-idx.service";
export declare class AppModule {
    private readonly _usuarioService;
    private readonly _usuarioIdxService;
    constructor(_usuarioService: UsuarioService, _usuarioIdxService: UsuarioIdxService);
    datos(): Promise<void>;
}
