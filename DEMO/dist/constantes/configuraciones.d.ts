export declare const CONFIGURACIONES: {
    bdd: {
        type: string;
        host: string;
        port: number;
        username: string;
        password: string;
        database: string;
        synchronize: boolean;
        dropSchema: boolean;
    };
    crearDatosTest: boolean;
};
