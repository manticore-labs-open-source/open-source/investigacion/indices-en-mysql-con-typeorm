"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const usuario_module_1 = require("../../usuario/usuario.module");
const usuario_idx_module_1 = require("../../usuario-con-indices/usuario-idx.module");
exports.ARREGLO_MODULOS = [
    usuario_module_1.UsuarioModule,
    usuario_idx_module_1.UsuarioIdxModule
];
//# sourceMappingURL=arreglo.modulos.js.map