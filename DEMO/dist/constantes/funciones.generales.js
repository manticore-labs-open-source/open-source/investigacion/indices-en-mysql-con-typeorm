"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const fs = require("fs");
exports.FUNCIONES_GENERALES = {
    crearDatos: (ruta, servicio) => {
        try {
            return new Promise(async (resolve, reject) => {
                fs.readFile('src/constantes/datos/' + ruta, 'utf8', (err, data) => {
                    if (err) {
                        reject({
                            mensaje: 'Error al leer',
                            error: err,
                        });
                    }
                    else {
                        servicio
                            .crear(JSON.parse(data))
                            .then(value => resolve(value))
                            .catch(reason => console.error(reason));
                    }
                });
            });
        }
        catch (e) {
            console.error('Error Función Crear Datos: ', e);
        }
    },
};
//# sourceMappingURL=funciones.generales.js.map