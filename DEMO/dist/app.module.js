"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
const common_1 = require("@nestjs/common");
const app_controller_1 = require("./app.controller");
const app_service_1 = require("./app.service");
const typeorm_1 = require("@nestjs/typeorm");
const usuario_service_1 = require("./usuario/usuario.service");
const usuario_idx_service_1 = require("./usuario-con-indices/usuario-idx.service");
const configuraciones_1 = require("./constantes/configuraciones");
const arreglo_entities_1 = require("./constantes/arreglo.entities");
const arreglo_modulos_1 = require("./constantes/datos/arreglo.modulos");
const funciones_generales_1 = require("./constantes/funciones.generales");
let AppModule = class AppModule {
    constructor(_usuarioService, _usuarioIdxService) {
        this._usuarioService = _usuarioService;
        this._usuarioIdxService = _usuarioIdxService;
        if (configuraciones_1.CONFIGURACIONES.crearDatosTest) {
            this.datos();
        }
        else {
            console.info('Datos no creados');
        }
    }
    async datos() {
        try {
            await funciones_generales_1.FUNCIONES_GENERALES.crearDatos('datos-usuario.json', this._usuarioService);
            await funciones_generales_1.FUNCIONES_GENERALES.crearDatos('datos-usuario.json', this._usuarioIdxService);
            console.info('Datos cargados correctamente');
        }
        catch (e) {
            console.error('Error al crear datos', e);
        }
    }
};
AppModule = __decorate([
    common_1.Module({
        imports: [
            typeorm_1.TypeOrmModule.forRoot({
                type: 'mysql',
                host: configuraciones_1.CONFIGURACIONES.bdd.host,
                port: configuraciones_1.CONFIGURACIONES.bdd.port,
                username: configuraciones_1.CONFIGURACIONES.bdd.username,
                password: configuraciones_1.CONFIGURACIONES.bdd.password,
                database: configuraciones_1.CONFIGURACIONES.bdd.database,
                entities: [...arreglo_entities_1.ARREGLO_ENTITIES],
                synchronize: configuraciones_1.CONFIGURACIONES.bdd.synchronize,
                dropSchema: configuraciones_1.CONFIGURACIONES.bdd.dropSchema,
            }),
            ...arreglo_modulos_1.ARREGLO_MODULOS,
        ],
        controllers: [app_controller_1.AppController],
        providers: [app_service_1.AppService],
    }),
    __metadata("design:paramtypes", [usuario_service_1.UsuarioService,
        usuario_idx_service_1.UsuarioIdxService])
], AppModule);
exports.AppModule = AppModule;
//# sourceMappingURL=app.module.js.map