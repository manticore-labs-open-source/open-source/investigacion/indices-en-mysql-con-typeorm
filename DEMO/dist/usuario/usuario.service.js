"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
const common_1 = require("@nestjs/common");
const typeorm_1 = require("@nestjs/typeorm");
const typeorm_2 = require("typeorm");
const usuario_entity_1 = require("./usuario.entity");
let UsuarioService = class UsuarioService {
    constructor(usuarioRepository) {
        this.usuarioRepository = usuarioRepository;
    }
    async crear(usuario) {
        try {
            const usuarioCreado = await this.usuarioRepository.save(usuario);
            return usuarioCreado;
        }
        catch (error) {
            throw {
                mensaje: 'No se pudo crear el usuario',
                codigo: common_1.HttpStatus.INTERNAL_SERVER_ERROR,
                error
            };
        }
    }
    async listarUsuarios() {
        try {
            const usuariosActivos = await this.usuarioRepository.find();
            return usuariosActivos;
        }
        catch (error) {
            throw {
                mensaje: 'No se pudo listar usuarios',
                codigo: common_1.HttpStatus.INTERNAL_SERVER_ERROR,
                error
            };
        }
    }
    async listarUsuariosActivos() {
        try {
            const usuariosActivos = await this.usuarioRepository.find({
                where: {
                    estaActivo: true
                }
            });
            return usuariosActivos;
        }
        catch (error) {
            throw {
                mensaje: 'No se pudo listar usuarios activos',
                codigo: common_1.HttpStatus.INTERNAL_SERVER_ERROR,
                error
            };
        }
    }
    async listarUsuariosConNombreIgualA(nombreABuscar) {
        try {
            const usuariosConNombreIgualA = await this.usuarioRepository.find({
                where: {
                    nombre: typeorm_2.Like(nombreABuscar),
                }
            });
            return usuariosConNombreIgualA;
        }
        catch (error) {
            throw {
                mensaje: 'No se pudo listar los usuarios con nombre igual a' + nombreABuscar,
                codigo: common_1.HttpStatus.INTERNAL_SERVER_ERROR,
                error
            };
        }
    }
};
UsuarioService = __decorate([
    common_1.Injectable(),
    __param(0, typeorm_1.InjectRepository(usuario_entity_1.UsuarioEntity)),
    __metadata("design:paramtypes", [typeorm_2.Repository])
], UsuarioService);
exports.UsuarioService = UsuarioService;
//# sourceMappingURL=usuario.service.js.map