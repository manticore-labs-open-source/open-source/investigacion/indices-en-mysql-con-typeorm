import { Repository } from 'typeorm';
import { UsuarioEntity } from "./usuario.entity";
export declare class UsuarioService {
    private usuarioRepository;
    constructor(usuarioRepository: Repository<UsuarioEntity>);
    crear(usuario: UsuarioEntity): Promise<UsuarioEntity>;
    listarUsuarios(): Promise<UsuarioEntity[]>;
    listarUsuariosActivos(): Promise<UsuarioEntity[]>;
    listarUsuariosConNombreIgualA(nombreABuscar: string): Promise<UsuarioEntity[]>;
}
