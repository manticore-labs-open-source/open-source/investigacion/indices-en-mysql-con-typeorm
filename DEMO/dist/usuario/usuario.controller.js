"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
const common_1 = require("@nestjs/common");
const usuario_service_1 = require("./usuario.service");
const usuario_entity_1 = require("./usuario.entity");
let UsuarioController = class UsuarioController {
    constructor(_usuarioService) {
        this._usuarioService = _usuarioService;
    }
    async getUsuarios() {
        try {
            const respuestaUsuario = await this._usuarioService.listarUsuarios();
            return respuestaUsuario;
        }
        catch (error) {
            throw new common_1.InternalServerErrorException(error);
        }
    }
    async getUsuariosHabilitados() {
        try {
            const respuestaUsuario = await this._usuarioService
                .listarUsuariosActivos();
            return respuestaUsuario;
        }
        catch (error) {
            throw new common_1.InternalServerErrorException(error);
        }
    }
    async getUsuariosNombre(nombreUsuario) {
        try {
            const respuestaUsuario = await this._usuarioService
                .listarUsuariosConNombreIgualA(nombreUsuario);
            return respuestaUsuario;
        }
        catch (error) {
            throw new common_1.InternalServerErrorException(error);
        }
    }
    async postCrearUsuario(usuario) {
        try {
            const respuestaUsuario = await this._usuarioService
                .crear(usuario);
            return respuestaUsuario;
        }
        catch (error) {
            throw new common_1.InternalServerErrorException(error);
        }
    }
};
__decorate([
    common_1.Get('listar-usuarios'),
    common_1.HttpCode(common_1.HttpStatus.OK),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", []),
    __metadata("design:returntype", Promise)
], UsuarioController.prototype, "getUsuarios", null);
__decorate([
    common_1.Get('listar-usuarios-habilitados'),
    common_1.HttpCode(common_1.HttpStatus.OK),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", []),
    __metadata("design:returntype", Promise)
], UsuarioController.prototype, "getUsuariosHabilitados", null);
__decorate([
    common_1.Get('listar-usuarios-nombre'),
    common_1.HttpCode(common_1.HttpStatus.OK),
    __param(0, common_1.Query('nombre')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [String]),
    __metadata("design:returntype", Promise)
], UsuarioController.prototype, "getUsuariosNombre", null);
__decorate([
    common_1.Post('crear-usuario'),
    common_1.HttpCode(common_1.HttpStatus.OK),
    __param(0, common_1.Body()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [usuario_entity_1.UsuarioEntity]),
    __metadata("design:returntype", Promise)
], UsuarioController.prototype, "postCrearUsuario", null);
UsuarioController = __decorate([
    common_1.Controller('usuario'),
    __metadata("design:paramtypes", [usuario_service_1.UsuarioService])
], UsuarioController);
exports.UsuarioController = UsuarioController;
//# sourceMappingURL=usuario.controller.js.map