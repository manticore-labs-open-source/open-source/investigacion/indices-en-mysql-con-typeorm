import { UsuarioService } from "./usuario.service";
import { UsuarioEntity } from "./usuario.entity";
export declare class UsuarioController {
    private readonly _usuarioService;
    constructor(_usuarioService: UsuarioService);
    getUsuarios(): Promise<UsuarioEntity[]>;
    getUsuariosHabilitados(): Promise<UsuarioEntity[]>;
    getUsuariosNombre(nombreUsuario: string): Promise<UsuarioEntity[]>;
    postCrearUsuario(usuario: UsuarioEntity): Promise<UsuarioEntity>;
}
