import { UsuarioIdxService } from "./usuario-idx.service";
import { UsuarioConIndicesEntity } from "./usuario-con-indices.entity";
export declare class UsuarioIdxController {
    private readonly _usuarioService;
    constructor(_usuarioService: UsuarioIdxService);
    getUsuariosConIdx(): Promise<UsuarioConIndicesEntity[]>;
    getUsuariosHabilitadosConIdx(): Promise<UsuarioConIndicesEntity[]>;
    getUsuariosNombreConIdx(nombreUsuario: string): Promise<UsuarioConIndicesEntity[]>;
    postCrearUsuarioIdx(usuario: UsuarioConIndicesEntity): Promise<UsuarioConIndicesEntity>;
}
