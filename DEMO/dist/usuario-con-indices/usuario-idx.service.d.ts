import { Repository } from 'typeorm';
import { UsuarioConIndicesEntity } from "./usuario-con-indices.entity";
export declare class UsuarioIdxService {
    private usuarioConIndicesRepository;
    constructor(usuarioConIndicesRepository: Repository<UsuarioConIndicesEntity>);
    crear(usuario: UsuarioConIndicesEntity): Promise<UsuarioConIndicesEntity>;
    listarUsuariosActivosConIndices(): Promise<UsuarioConIndicesEntity[]>;
    listarUsuariosConIndices(): Promise<UsuarioConIndicesEntity[]>;
    listarUsuariosConNombreIgualAConIndices(nombreABuscar: string): Promise<UsuarioConIndicesEntity[]>;
}
